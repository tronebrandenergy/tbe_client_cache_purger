<?php

namespace Drupal\tbe_client_cache_purger_cloudflare\Controller;

use CloudFlarePhpSdk\ApiEndpoints\ZoneApi;
use Drupal\tbe_client_cache_purger\Controller\BasePurgerController;
use Symfony\Component\HttpFoundation\Request;

class CloudflarePurgerController extends BasePurgerController {

  const PURGER_MODULE_NAME = 'tbe_client_cache_purger_cloudflare';
  const CLOUDFLARE_CONFIG_NAME = 'cloudflare.settings';

  public function __construct() {
    $this->cacheType = 'Cloudflare';
  }

  /**
   * Method to do the actual cache purging.
   */
  protected function purgeCache(Request $request) {
    $config = $this->config(self::CLOUDFLARE_CONFIG_NAME);

    // Cloudflare settings
    $cloudflareApiKey = $config->get('apikey');
    $cloudflareEmail = $config->get('email');
    $cloudflareZoneId = $config->get('zone_id');

    try {
      // Purge all cache using Cloudflare's API
      $zoneApi = new ZoneApi($cloudflareApiKey, $cloudflareEmail);
      $zoneApi->purgeAllFiles($cloudflareZoneId);

      // Log message
      $this->getLogger(self::PURGER_MODULE_NAME)->notice('%type cache purged successfully! IP: %ip', [
        '%type' => $this->cacheType,
        '%ip' => $request->getClientIp(),
      ]);
    } catch (\Exception $e) {
      $errorMessage = $this->t('Failed attempt to purge %type cache. IP: %ip. Additional information: %info', [
        '%type' => $this->cacheType,
        '%ip' => $request->getClientIp(),
        '%info' => $e->getMessage(),
      ]);

      // Log message
      $this->getLogger(self::PURGER_MODULE_NAME)->error($errorMessage);

      // Error response
      throw new \Exception(strip_tags($errorMessage));
    }
  }

}
