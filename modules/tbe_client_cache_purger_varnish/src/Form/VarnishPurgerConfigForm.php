<?php

namespace Drupal\tbe_client_cache_purger_varnish\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class VarnishPurgerConfigForm extends ConfigFormBase {

  const CONFIG_NAME = 'tbe_client_cache_purger_varnish.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'client_cache_purger_varnish_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

  /**
   * Returns this modules configuration object.
   */
  protected function getConfig() {
    return $this->config(self::CONFIG_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfig();

    $form['ban_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('BAN Endpoint'),
      '#default_value' => $config->get('ban_endpoint'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfig();
    $values = $form_state->getValues();
    foreach ($values as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();
    $this->messenger()->addMessage('Configuration changes were successfully saved.');
  }

}
