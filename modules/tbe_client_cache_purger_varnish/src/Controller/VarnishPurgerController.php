<?php

namespace Drupal\tbe_client_cache_purger_varnish\Controller;

use Drupal\tbe_client_cache_purger\Controller\BasePurgerController;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Request;

class VarnishPurgerController extends BasePurgerController {

  const PURGER_MODULE_NAME = 'tbe_client_cache_purger_varnish';
  const PURGER_CONFIG_NAME = 'tbe_client_cache_purger_varnish.settings';

  public function __construct() {
    $this->cacheType = 'Varnish';
  }

  /**
   * Method to do the actual cache purging.
   */
  protected function purgeCache(Request $request) {
    $config = $this->config(self::PURGER_CONFIG_NAME);

    // Get all configured Varnish hosts
    $allHosts = $config->get('ban_endpoint');
    if (!$allHosts) {
      $allHosts = \Drupal::request()->getSchemeAndHttpHost();
    }

    // For each Varnish host, send a BAN request for the local website's host
    $client = new Client();
    $hosts = explode(',', $allHosts);
    foreach ($hosts as $host) {
      $host = trim($host);

      try {
        // HTTP BAN Request
        $response = $client->request('BAN', $host, [
          'headers' => [
            'Host' => $_SERVER['HTTP_HOST'],
          ]
        ]);

        // Log message
        $this->getLogger(self::PURGER_MODULE_NAME)->notice('%type cache purged successfully in %host! IP: %ip. Response status: %response', [
          '%type' => $this->cacheType,
          '%host' => $host,
          '%ip' => $request->getClientIp(),
          '%response' => $response->getStatusCode(),
        ]);
      } catch (GuzzleException $guzzleException) {
        $errorMessage = $this->t('Failed attempt to purge %type cache in %host. IP: %ip. Additional information: %info', [
          '%type' => $this->cacheType,
          '%host' => $host,
          '%ip' => $request->getClientIp(),
          '%info' => $guzzleException->getMessage(),
        ]);

        // Log message
        $this->getLogger(self::PURGER_MODULE_NAME)->error($errorMessage);

        // Error response
        throw new \Exception(strip_tags($errorMessage));
      }
    } // foreach
  }

}
