<?php

namespace Drupal\tbe_client_cache_purger\Commands;

use Drush\Commands\DrushCommands;
use Drupal\tbe_client_cache_purger\Controller\LocalPurgerController;

class ClientCachePurgerCommands extends DrushCommands {

  /**
   * Purge all cache layers enabled on the website.
   *
   * @command tbe_client_cache_purger:purge-all-cache
   * @aliases ccp-purge-all-cache
   * @usage tbe_client_cache_purger:purge-all-cache
   */
  public function purgeAllCache() {
    $localPurger = new LocalPurgerController();
    $outputMessages = $localPurger->purgeAllCache(FALSE);
    foreach ($outputMessages as $message) {
      $this->output()->writeln($message);
    }
  }

}
