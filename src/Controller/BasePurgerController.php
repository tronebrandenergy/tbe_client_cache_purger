<?php

namespace Drupal\tbe_client_cache_purger\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BasePurgerController extends ControllerBase {

  const MODULE_NAME = 'tbe_client_cache_purger';
  const CONFIG_NAME = 'tbe_client_cache_purger.settings';

  /**
   * @var string The authentication hash to verify purge call.
   */
  protected $hashedToken;

  /**
   * @var string Type of cache being purged: Drupal, Varnish etc.
   */
  protected $cacheType;

  public function __construct() {
    $this->cacheType = 'Generic';
  }

  /**
   * Implements the main logic to purge the cache.
   */
  public function purge(Request $request) {
    // Generate expected token
    $this->generateHashedToken();

    // Check if request is valid
    if (!$this->isValidRequest($request)) {
      // Error response
      return new Response('Invalid request to purge ' . $this->cacheType . ' cache.', Response::HTTP_OK);
    }

    try {
      // Purge the cache
      $this->purgeCache($request);

      // Success response
      return new Response($this->cacheType . ' cache purged successfully!', Response::HTTP_OK);
    } catch (\Exception $e) {
      // Success response
      return new Response($e->getMessage(), Response::HTTP_OK);
    }
  }

  /**
   * Implements the main logic to purge the cache from a local method call.
   */
  public function purgeLocal(Request $request) {
    try {
      // Purge the cache
      $this->purgeCache($request);
      return TRUE;
    } catch (\Exception $e) {
      \Drupal::messenger()->addMessage($e->getMessage(), 'error');
    }
    return FALSE;
  }

  /**
   * Calculates the expected hashed token.
   */
  protected function generateHashedToken() {
    $config = $this->config(self::CONFIG_NAME);
    $this->hashedToken = hash_hmac($config->get('algorithm'), $config->get('token'), date('Y-m-d') . $config->get('salt'));
  }

  /**
   * Validates the request and its token.
   */
  protected function isValidRequest(Request $request) {
    // Check if the request has a token in the header
    if (!$request->headers || !$request->headers->has('token')) {
      $this->getLogger(self::MODULE_NAME)->error('Failed attempt to purge %type cache. No token provided in the request. IP: %ip', [
        '%type' => $this->cacheType,
        '%ip' => $request->getClientIp(),
      ]);
      return FALSE;
    }

    // Check if the header is valid
    $clientToken = $request->headers->get('token');
    if (!hash_equals($this->hashedToken, $clientToken)) {
      $this->getLogger(self::MODULE_NAME)->error('Failed attempt to purge %type cache. Invalid token: %token. IP: %ip', [
        '%type' => $this->cacheType,
        '%token' => $clientToken,
        '%ip' => $request->getClientIp(),
      ]);
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Method to do the actual cache purging.
   */
  protected function purgeCache(Request $request) {
    // Should be implemented in subclasses
  }

}
