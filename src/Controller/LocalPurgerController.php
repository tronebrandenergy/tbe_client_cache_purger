<?php

namespace Drupal\tbe_client_cache_purger\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\tbe_client_cache_purger_cloudflare\Controller\CloudflarePurgerController;
use Drupal\tbe_client_cache_purger_varnish\Controller\VarnishPurgerController;

class LocalPurgerController extends ControllerBase {

  /**
   * Method to do the actual cache purging.
   */
  public function purgeAllCache($printOutput = TRUE) {
    $moduleHandler = \Drupal::service('module_handler');
    $request = \Drupal::request();

    // Output messages
    $outputMessages = [];

    // 1) Purge Drupal cache
    drupal_flush_all_caches();
    $outputMessages[] = 'Drupal cache purged successfully!';

    // 2) Purge Varnish cache, if module enabled
    if ($moduleHandler->moduleExists('tbe_client_cache_purger_varnish')) {
      $varnishController = new VarnishPurgerController();
      if ($varnishController->purgeLocal($request)) {
        $outputMessages[] = 'Varnish cache purged successfully!';
      }
    }

    // 3) Purge Cloudflare cache, if module enabled
    if ($moduleHandler->moduleExists('tbe_client_cache_purger_cloudflare')) {
      $cloudflareController = new CloudflarePurgerController();
      if ($cloudflareController->purgeLocal($request)) {
        $outputMessages[] = 'Cloudflare cache purged successfully!';
      }
    }

    // Print or return output messages
    if ($printOutput) {
      $messenger = \Drupal::messenger();
      foreach ($outputMessages as $message) {
        $messenger->addMessage($message);
      }
    } else {
      return $outputMessages;
    }
  }

}
