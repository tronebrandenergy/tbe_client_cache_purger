<?php

namespace Drupal\tbe_client_cache_purger\Controller;

use Symfony\Component\HttpFoundation\Request;

class DrupalPurgerController extends BasePurgerController {

  public function __construct() {
    $this->cacheType = 'Drupal';
  }

  /**
   * Method to do the actual cache purging.
   */
  protected function purgeCache(Request $request) {
    // Purge Drupal caches
    drupal_flush_all_caches();

    // Log message
    $this->getLogger(parent::MODULE_NAME)->notice('%type cache purged successfully! IP: %ip', [
      '%type' => $this->cacheType,
      '%ip' => $request->getClientIp(),
    ]);
  }

}
