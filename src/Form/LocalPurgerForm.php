<?php

namespace Drupal\tbe_client_cache_purger\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tbe_client_cache_purger\Controller\LocalPurgerController;

class LocalPurgerForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'client_cache_purger_local_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Module handler
    $moduleHandler = \Drupal::service('module_handler');

    // Array with all the enabled Cache Purger modules
    $cacheModulesEnabled = ['Drupal'];
    if ($moduleHandler->moduleExists('tbe_client_cache_purger_varnish')) {
      $cacheModulesEnabled[] = 'Varnish';
    }
    if ($moduleHandler->moduleExists('tbe_client_cache_purger_cloudflare')) {
      $cacheModulesEnabled[] = 'Cloudflare';
    }

    // Form
    $form['markup'] = [
      '#type' => 'markup',
      '#markup' => '<p>Clicking the button below will clear the following cache layers: <b>' . implode(', ', $cacheModulesEnabled) . '</b></p>',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear all caches'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $localPurger = new LocalPurgerController();
    $localPurger->purgeAllCache();
  }

}
