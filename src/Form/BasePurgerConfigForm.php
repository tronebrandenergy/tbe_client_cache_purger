<?php

namespace Drupal\tbe_client_cache_purger\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class BasePurgerConfigForm extends ConfigFormBase {

  const CONFIG_NAME = 'tbe_client_cache_purger.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'client_cache_purger_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

  /**
   * Returns this modules configuration object.
   */
  protected function getConfig() {
    return $this->config(self::CONFIG_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfig();

    $form['salt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hash Salt'),
      '#default_value' => $config->get('salt'),
      '#required' => TRUE,
    ];

    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hash Token'),
      '#default_value' => $config->get('token'),
      '#required' => TRUE,
    ];

    $form['algorithm'] = [
      '#type' => 'select',
      '#title' => $this->t('Hash Algorithm'),
      '#options' => [
        'sha256' => $this->t('sha256 (recommended)'),
        'sha384' => $this->t('sha384'),
        'sha512' => $this->t('sha512'),
        'haval160,4' => $this->t('haval160,4'),
      ],
      '#default_value' => $config->get('algorithm'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfig();
    $values = $form_state->getValues();
    foreach ($values as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();
    $this->messenger()->addMessage('Configuration changes were successfully saved.');
  }

}
